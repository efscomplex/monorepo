import styled from 'styled-components'

export const Layout = styled('div')`
  display: grid;
  grid-template-columns: ${(props) => props.cols};
  grid-template-rows: ${(props) => props.rows};
`
