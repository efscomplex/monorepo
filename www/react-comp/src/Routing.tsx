import React from 'react'
import {
  Switch,
  BrowserRouter as Router,
  Route,
  Redirect,
} from 'react-router-dom'

export type Page = {
  comp: React.ReactNode
  path: string
  label: string
  to: string
}

export interface IRouting {
  pages: Page[]
  defaultRoute?: string
  hash?: string
}

export const Routing: React.FC<IRouting> = ({
  pages,
  children,
  defaultRoute = '/',
}) => {
  const routes = pages.map((page: Page) => (
    <Route exact path={page.path}>
      {page.comp}
    </Route>
  ))
  const jsxRoutes = React.Children.toArray(routes)

  return (
    <Router>
      {typeof children !== 'function' && children}
      <Switch>
        {typeof children === 'function' ? children(jsxRoutes) : jsxRoutes}
        <Route>
          <Redirect to={defaultRoute} />
        </Route>
      </Switch>
    </Router>
  )
}
