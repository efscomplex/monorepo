import React from 'react'

export const Greetings: React.FC<{ name: string }> = ({ name }) => {
	return <div>greetings to {name}</div>
}
