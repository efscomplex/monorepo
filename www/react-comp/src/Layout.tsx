import React from 'react'

type Grid = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12

type Props = {
  rows?: Grid
  cols?: number
  style?: any
  className?: string
}

const Layout: React.FC<Props> = ({
  children,
  style,
  rows = 1,
  cols = 1,
  className,
}) => {
  const css = {
    ...style,
    display: 'grid',
    gridTemplateColumns: `repeat(${cols},1fr)`,
    gridTemplateRows: rows,
  }
  return (
    <div style={css} className={className}>
      {children}
    </div>
  )
}
export { Layout }
