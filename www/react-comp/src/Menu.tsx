import React from 'react'
import { NavLink } from 'react-router-dom'
import { Page } from './Routing'

interface Props {
  items: Page[]
}

export const Menu: React.FC<Props> = ({ items }) => {
  return (
    <nav>
      {items.map((item) => {
        const path = item.to || item.path
        return (
          <NavLink key={path} to={path}>
            {item.label}
          </NavLink>
        )
      })}
    </nav>
  )
}
